import { cloudProvider } from "$lib/tidy/stores/app.store";
import { get } from "svelte/store";
import { persistLocally, retrieveLocally } from "../../tidy/stores/persistance";
import { Cloud } from "../../tidy/types/cloud.enum";
import { ItemType } from "../../tidy/types/item.enum";

export class DayPersistance {
  retrieveDay(sessionId: number) {
    switch (get(cloudProvider)) {
      case Cloud.local:
        let sessions = retrieveLocally(ItemType.Sessions);
        if (!sessions) return;
        let sessionIndex = sessions.findIndex(
          (session: { id: number }) => session.id === sessionId
        );
        return sessions[sessionIndex].tasks;
      default:
        return [];
    }
  }
}
