import { goto } from "$app/navigation";
import { get } from "svelte/store";
import { userLocalPreferences } from "../stores/local.store";
import { TimerMode } from "../types/timerMode.enum";

export function resolvePointPageMode() {
  if (get(userLocalPreferences).timerMode === TimerMode.JOURNAL) {
    goto("/point/journal");
  } else {
    goto("/point/minimal");
  }
}
